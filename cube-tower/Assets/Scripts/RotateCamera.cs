﻿using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    public float speedRotate = 10f;

    private Transform rotateCamera;
    private void Start()
    {
        /* get Component Camera*/
        rotateCamera = GetComponent<Transform>();
    }

    private void Update()
    {
        /* rotate Camera*/
        rotateCamera.Rotate(0, speedRotate * Time.deltaTime, 0);
    }
}